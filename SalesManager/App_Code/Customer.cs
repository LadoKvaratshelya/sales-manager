﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Customer
/// </summary>
public class Customer
{
    public Customer()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public string Name { get; set; }

    public string Email { get; set; }


    public Customer(string name, string email)
    {
        this.Name = name;
        this.Email = email;
    }
}