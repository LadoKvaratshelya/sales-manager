﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{

    static List<Customer> customersList = new List<Customer>();

    // variable to store selected item id
    static string idToEdit = "";

    // variable to distinguish between edit and save
    static bool edit = false;

    protected void Page_Load(object sender, EventArgs e)
    {
        GridViewTotalSales.Visible = false;
        GridViewMaxSales.Visible = false;
    }

    protected void ButtonAdd_Click(object sender, EventArgs e)
    {

    }

    protected void btnSaveData_Click(object sender, EventArgs e)
    {
        Customer customer = new Customer(NameTextBox.Text, EmailTextBox.Text);

        if (edit)
        {
            editData(customer);
            edit = false;
        }
        else
            saveData(customer);
    }

    public void saveData(Customer customer)
    {
        if (!customerExists(customer))
            customersList.Add(customer);

        updateDropdownList();

        clearTextFields();
    }

    public void editData(Customer customer)
    {
        saveToCustomer(customer);

        updateDropdownList();

        clearTextFields();
    }

    // elliminates duplicate customers
    public bool customerExists(Customer customer)
    {
        foreach(var cust in customersList)
        {
            if (cust.Email == customer.Email)
                return true;
        }

        return false;
    }

    protected void CustomersDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        string selectedValue = CustomersDropDownList.SelectedItem.Value.ToString();

        // allow edit of selected form dropdown list item except of the first one
        if(selectedValue != "First")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);

            NameTextBox.Text = extractName(CustomersDropDownList.SelectedItem.Text);
            EmailTextBox.Text = selectedValue;
            idToEdit = selectedValue; // store selected item ID
        }

        edit = true;
    }

    public string extractName(string fieldText)
    {
        if(fieldText.Length > 0)
        {
            int index = fieldText.IndexOf(',');

            return fieldText.Substring(0, index);
        }

        return "";
    }

    public void saveToCustomer(Customer customer)
    {
        foreach (var cust in customersList)
        {
            if (cust.Email == idToEdit)
            {
                cust.Email = customer.Email;
                cust.Name = customer.Name;
                break;
            }
        }
    }

    public void updateDropdownList()
    {
        CustomersDropDownList.Items.Clear();

        CustomersDropDownList.Items.Insert(0, new ListItem("cmbCustomers", "First"));

        for (int i = 0; i < customersList.Count; i++)
        {
            CustomersDropDownList.Items.Insert(i + 1, new ListItem(customersList[i].Name + ", " + customersList[i].Email, customersList[i].Email));
        }
    }

    // clear text fields after the data is saved
    public void clearTextFields()
    {
        NameTextBox.Text = "";
        EmailTextBox.Text = "";
    }

    protected void ButtonTotalSales_Click(object sender, EventArgs e)
    {
        GridViewTotalSales.Visible = true;
        GridViewMaxSales.Visible = false;

        using (var db = new SalesDatabaseEntities())
        {
            var result = from p in db.Users
                         join s in db.Sales on p.UserId equals s.UserId
                         join sp in db.SaleProducts on s.SaleId equals sp.SaleId
                         select new
                         {
                             Seller = p.Name,
                             ProductType = s.Type,
                             Sales = sp.Price,
                         };
            GridViewTotalSales.DataSource = result.ToList();

            GridViewTotalSales.DataBind();
        }
    }

    protected void ButtonMaxSales_Click(object sender, EventArgs e)
    {
        
    }
}