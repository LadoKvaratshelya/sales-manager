﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" EnableEventValidation="false" ValidateRequest="false" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

    <script type="text/javascript">
    function openModal() {
        $('#MyModal').modal('show');
    }
</script>
</head>
<body>
    <form id="form1" runat="server">
        <div style="padding: 25px 25px 25px 25px">
            <asp:DropDownList ID="CustomersDropDownList" runat="server" AutoPostBack = "true" AppendDataBoundItems="true" OnSelectedIndexChanged="CustomersDropDownList_SelectedIndexChanged">
            <asp:ListItem Text="cmbCustomers" Selected="True" Value="0"/>
            </asp:DropDownList>
            <asp:Button ID="ButtonAdd" runat="server" Text="+" OnClick="ButtonAdd_Click" CssClass="btn btn-info btn-lg" data-toggle="modal" data-target="#MyModal" OnClientClick ="return false"/>
        </div>

        <div class="modal" id="MyModal" role="dialog">
            <div class="modal-dialog">
                <div class=" modal-content">
                    <div class=" modal-header">
                        <button class="btn btn-primary" data-dismiss="modal" style="float: right;">X</button>
                        <h4>New Customer</h4>
                    </div>
                    <div class="modal-body"> </div>
                    <asp:Label ID="Label1" runat="server" Text="Name"></asp:Label>
                    <asp:TextBox ID="NameTextBox" runat="server"></asp:TextBox>
                    <asp:Label ID="Label2" runat="server" Text="Email"></asp:Label>
                    <asp:TextBox ID="EmailTextBox" runat="server"></asp:TextBox>

                    <asp:Button ID="btnSaveData" runat="server" onclick="btnSaveData_Click" Text="Save"/>

                </div>

            </div>
        </div>

        <asp:ScriptManager ID="ScrMang1" runat="server"></asp:ScriptManager>

        <div style="padding: 25px 25px 25px 25px">
            <asp:Button ID="ButtonTotalSales" runat="server" Text="Display Total Sales" OnClick="ButtonTotalSales_Click"/>
            <asp:Button ID="ButtonMaxSales" runat="server" Text="Display Max Sales" OnClick="ButtonMaxSales_Click"/>
        </div>
        <div></div>
        <div style="padding: 25px 25px 25px 25px">
            <asp:GridView ID="GridViewTotalSales" runat="server"></asp:GridView>
            <asp:GridView ID="GridViewMaxSales" runat="server"></asp:GridView>

        </div>
    </form>
</body>
</html>
